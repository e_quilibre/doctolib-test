class Event < ActiveRecord::Base
  STEP = 1800
  private_constant :STEP

  scope :appointments_next_seven_days, ->(date) { where("kind = 'appointment' and starts_at > ? and starts_at < ?", date, date+7).pluck(:starts_at, :ends_at) }
  scope :availabilities_next_seven_days, ->(date) { where("kind = 'opening' and weekly_recurring='f' and starts_at > ? and starts_at < ?", date, date+7).pluck(:starts_at, :ends_at) }
  scope :reccuring_within_next_seven_days, ->(date) { where("weekly_recurring='t' and starts_at < ?", date+7).pluck(:starts_at, :ends_at) }

  class << self
    def availabilities date
      range = make_days_in_range(from_date: date)

      search_slots_availables(
        from_range: range,
        avaibilities: openings(from_date: date, range: range),
        appointments: create_date_slots(records: appointments_next_seven_days(date))
      )
    end

    # private

    def search_slots_availables(from_range:, avaibilities:, appointments:)
      raw_schedule = make_empty_schedule(range: from_range)
      avaibilities.map { |avb| raw_schedule[avb[0]] = avb[1] }
      appointments.map do |apt|
        slots = raw_schedule[apt[0]]
        unless slots.empty?
          apt[1].map { |s| slots.delete(s) }
        end
      end
      reformat(schedule: raw_schedule)
    end


    def reformat(schedule:)
      results = []
      schedule.each_pair { |date, slots| results << { date: DateTime.parse(date), slots: hours_list(array: slots) }   }
      results
    end


    def hours_list(array:)
      unless array.empty?
        array.map! { |t| Time.at(t).utc.strftime("%-H:%M") }
      end
      array
    end


    def make_empty_schedule(range:)
      h = {}
      range.each { |d| h[d.strftime('%Y-%m-%d')] = [] }
      h
    end


    def make_days_in_range(from_date:)
        Range.new(from_date, from_date+7, true).freeze
    end


    def openings(from_date:, range:)
      create_date_slots(records: availabilities_next_seven_days(from_date)).concat(reccurings(from_date: from_date, range: range))
    end


    def create_time_slots(date_start:, date_end:)
      time_slots = []
      ( date_start.to_time.to_i ... date_end.to_time.to_i ).step(STEP) { |t| time_slots << t }
      [
        date_start.strftime('%Y-%m-%d'),
        time_slots
      ]
    end


    def create_date_slots(records:)
      converted = []
      records.map { |r| converted << create_time_slots(date_start: r[0], date_end: r[1]) }
      converted
    end


    def reccurings(from_date:, range:)
      update_reccuring(raw_reccurings: reccuring_within_next_seven_days(from_date), range: range)
    end


    def update_reccuring(raw_reccurings:, range:)
      raw_reccurings.map do |r|
        incr = 0
        while !range.cover?(r[0]+incr)
          incr += 7.days
        end
        create_time_slots(date_start:r[0]+incr, date_end:r[1]+incr)
      end
    end

  end # Class methods
end # Event
